FROM registry.opensuse.org/opensuse/tumbleweed:latest
RUN zypper --quiet --non-interactive install --type pattern devel_basis; \
    zypper --quiet --non-interactive install lazarus wget unzip git python39; \
    zypper clean --all; \
    rm -f /usr/bin/python3; \
    ln -s /usr/bin/python3.9 /usr/bin/python3; \
    git clone https://github.com/Warfley/LazarusPackageManager.git /opt/LazarusPackageManager; \
    groupadd lazarus && useradd -m --no-log-init -g lazarus lazarus; 
USER lazarus:lazarus
ENV PATH=/opt/LazarusPackageManager:$PATH
RUN lpm update; \
    lpm lazarus add $(lazbuild -v) /usr/lib64/lazarus; \
    lpm install Indy10; \
    lpm install dexif; \
    lpm install ZeosDBO; \
    lazbuild ~/.lpm/packages/Indy10/Indy10/indylaz.lpk; \
    lazbuild ~/.lpm/packages/dexif/dexif/dexif_package.lpk; \
    lazbuild ~/.lpm/packages/ZeosDBO/zeosdbo/packages/lazarus/*.lpk ; \
    lazbuild  --build-ide=; 
